/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

/**
 *
 * @author Lenovo
 */
public class Friend {
    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastID=1;

    public Friend(String name, int age, String tel) {
        this.id = lastID++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Friend{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }
    
    public static void main(String[] args) {
        Friend f1 = new Friend("Pom",70, "0811111");
        System.out.println(f1);
    }
}
